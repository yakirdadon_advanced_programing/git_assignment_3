#include "sqlite3.h"
//#include "sqlite3.c"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#define ARR_SIZE 3
#define FILE_NAME "FirstPart.db"

using std::string;
using std::cout;
using std::endl;
using std::vector;
using std::unordered_map;
using std::pair;

unordered_map<string, vector<string>> results;

void clearTable();
void command(string com, int* rc, sqlite3** db);
int callback(void* notUsed, int argc, char** argv, char** azCol);

void printTable();

int main()
{
	int rc;
	sqlite3* db;
	bool flag = true;
	string names[ARR_SIZE] = { "Moses", "Rick", "Morty" };
	// connection to the database
	rc = sqlite3_open(FILE_NAME, &db);
	if (rc != SQLITE_OK)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	command("create table people(id integer PRIMARY KEY AUTOINCREMENT, name string);", &rc , &db);
	if (rc != SQLITE_OK)
	{
		system("Pause");
		return 1;
	}
	system("Pause");
	system("CLS");

	//loop to insert people
	for (int i = 0; i < ARR_SIZE; i++)
	{
		command("insert into people(name) values(\"" + names[i] + "\");", &rc, &db);
		
		if (rc != SQLITE_OK)
		{
			system("Pause");
			return 1;
		}
	}

	command("select * from people;", &rc, &db);
	printTable();
	system("Pause");
	system("CLS");

	command("update people set name = \"Evil_Morty\" where id = 3;", &rc, &db);

	if (rc != SQLITE_OK)
	{
		system("Pause");
		return 1;
	}
	command("select * from people;", &rc, &db);
	system("pause");
	sqlite3_close(db);
}

void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}
/*Take care of everything with the send including print before and handeling eror.
com-sqlite command.
*/
void command(string com, int* rc, sqlite3** db)
{
	char *zErrMsg = 0;
	cout << com << endl;

	*rc = sqlite3_exec(*db, com.c_str(), callback, &printTable, &zErrMsg);

	if (*rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	for (i = 0; i < argc; i++)
	{

		cout << azCol[i] << "=" << argv[i] << endl;
	}

	return 0;
}

void printTable()
{
	auto iter = results.end();
	iter--;
	// int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}