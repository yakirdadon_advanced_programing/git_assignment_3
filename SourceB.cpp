#include "sqlite3.h"
//#include "sqlite3.c"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#define FILE_NAME "carsDealer.db"

using std::string;
using std::cout;
using std::endl;
using std::vector;
using std::unordered_map;
using std::pair;
using std::to_string;

string info;//global var to get info from printing func(called from exec func).

unordered_map<string, vector<string>> results;
void command(string com, int* rc, sqlite3** db, char *zErrMsg);
int callback(void* notUsed, int argc, char** argv, char** azCol);
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
void printTable();
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);

int main()
{
	int rc;
	sqlite3* db;
	bool flag = true;
	char *zErrMsg = 0;

	// connection to the database
	rc = sqlite3_open(FILE_NAME, &db);
	if (rc != SQLITE_OK)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	//test carPurchase
	carPurchase(7, 20, db, zErrMsg);//dont work
	system("Pause");
	carPurchase(12, 15, db, zErrMsg);//work
	system("Pause");
	carPurchase(11, 22, db, zErrMsg);//work
	system("pause");

	//test balanceTransfer
	balanceTransfer(2, 3, 14001, db, zErrMsg);
	system("pause");
	balanceTransfer(2, 3, 1400, db, zErrMsg);
	system("pause");
	sqlite3_close(db);
}

/*buy a car
includes: update buyer account and update car availablity(with all necessary checks).
*/
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	system("CLS");//clean screen
	int rc;
	//Check if buyer balance bigger or equal to the car price.
	command("select available from cars where id = " + to_string(carid) + " and (select balance from accounts where buyer_id = " + to_string(buyerid) + ") > (select price from cars where id = " + to_string(carid) + ") or (select balance from accounts where buyer_id = " + to_string(buyerid) + ") = (select price from cars where id = " + to_string(carid) + ");", &rc, &db, zErrMsg);
	
	cout << info << endl;
	system("pause");
	if (info == "1")//if available
	{
		//buyer balance-car price
		command("update accounts set balance= (select balance from accounts where id = "+to_string(buyerid)+" )-(select price from cars where id = "+ to_string(carid)+ ")where Buyer_id = "+ to_string(buyerid)+" ;",&rc, &db, zErrMsg);
	
		if (rc != SQLITE_OK)
		{
			system("Pause");
			return 1;
		}
		//car:available = 1 => car:available = 0//make car unavailable.
		command("update cars set available = 0 where id = " + to_string(carid) + " ;", &rc, &db, zErrMsg);
		if (rc != SQLITE_OK)
		{
			system("Pause");
			return 1;
		}
		cout << "The car was bought." << endl;
		return true;
	}
	cout << "The car was'nt bought." << endl;
	return false;
}

//sqlite version to check
//update accounts set balance = ((select balance from accounts where id = 10) - (select price from cars where id = 20))where Buyer_id = 10;
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void command(string com, int* rc, sqlite3** db, char *zErrMsg)
{
	cout << com << endl;
	*rc = sqlite3_exec(*db, com.c_str(), callback,0 , &zErrMsg);
	if (*rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	info = argv[0];
	/*for (i = 0; i < argc; i++)
	{

		cout << azCol[i] << "=" << argv[i] << endl;
		
	}*/

	return 0;
}

void printTable()
{
	auto iter = results.end();
	iter--;
	// int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}
/*transfer money between accounts
include: taking money from buyer (check if balance is big enough(equal or bigger)) and adding the money that was taken into the getter account/balance.
from and to are used as id's.
*/
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	system("CLS");//clean screen.
	int rc;
	//check if giver balance is bigger or equal to money needed to be transfered.
	command("select balance from accounts where buyer_id = " + to_string(from) + " and (select balance from accounts where buyer_id = " + to_string(from) + ") > " + to_string(amount) + " or (select balance from accounts where buyer_id = " + to_string(from) + ") = " + to_string(amount) + "  ;", &rc, &db, zErrMsg);
	
	if (rc != SQLITE_OK)
	{
		system("Pause");
		return 1;
	}
	cout << "-" << info << endl;//see how much left in the account if transfer is possible and if not possible show nothing.
	if (!info.empty())//if everythng is good.
	{
		//take money from giver.
		command("update accounts set balance = ( (select balance from accounts where buyer_id = " + to_string(from) + ") - " + to_string(amount) + ") where buyer_id = " + to_string(from) + ";", &rc, &db, zErrMsg);
		
		if (rc != SQLITE_OK)
		{
			system("Pause");
			return 1;
		}
		//give money to taker.
		command("update accounts set balance = ( (select balance from accounts where buyer_id = " + to_string(to) + ") +  " + to_string(amount) + ") where buyer_id = " + to_string(to) + " ;", &rc, &db, zErrMsg);

		if (rc != SQLITE_OK)
		{
			system("Pause");
			return 1;
		}
		cout << "Money was transferd." << endl;
		return true;
	}
	cout << "Money wasn't transferd." << endl;
	return false;
}
